const { db } = require('../models')

const FindUser = ({id})=>{
    // return {name:'Haruto', age:'26'}
    try {
        if(db.some((user)=> user.id === id)){
            const fundUser = db.filter((user)=>user.id === id )
            return { statusCode:200, data: fundUser}
        }
        return { statusCode:400, message: 'No fue posible encontrar el usuario'}
    } catch (error) {
        console.log({step: 'service Service', error: error.toString()})
        return { statusCode: 500, message: error.toString()}
    }
}
module.exports = {
    FindUser,
}