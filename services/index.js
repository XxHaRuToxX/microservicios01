const { FindUser} = require('../controllers')

const service = ({id})=>{
    try {

        let { statusCode, data, message } = FindUser({id})
        
        return { statusCode, data, message }
        
    } catch (error) {
        console.log({step: 'service Service', error: error.toString()})
        return { statusCode: 500, message: error.toString()}
    }

}
module.exports = {
    service,
}